package com.mytweeter.app.mapper;


import com.mytweeter.app.model.Tweet;
import com.mytweeter.app.model.dto.TweetDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface TweetMapper {
    @Mappings(value = {
            @Mapping(source = "id", target = "tweetId"),
            @Mapping(source = "created", target = "tweetCreationTime"),
            @Mapping(source = "status", target = "tweetStatus"),
            @Mapping(source = "text", target = "tweetContent"),
            @Mapping(source = "type", target = "tweetType"),
            @Mapping(source = "tagSet", target = "tagSet"),
    })
    TweetDto tweetToTweetDto(Tweet tweet);

    @Mappings(value = {
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "created", ignore = true),
            @Mapping(target = "status", constant = "ORIGINAL"),
            @Mapping(target = "text", source = "tweetContent"),
            @Mapping(target = "type", source = "tweetType"),
    })
    Tweet tweetDtoToNewTweet(TweetDto dto);

    @Mappings(value = {
            @Mapping(target = "id", source = "tweetId"),
            @Mapping(target = "created", source = "tweetCreationTime"),
            @Mapping(target = "status", constant = "EDITED"),
            @Mapping(target = "text", source = "tweetContent"),
            @Mapping(target = "type", source = "tweetType"),
    })
    Tweet tweetDtoToTweet(TweetDto dto);
}
