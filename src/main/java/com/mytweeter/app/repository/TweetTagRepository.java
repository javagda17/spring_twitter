package com.mytweeter.app.repository;

import com.mytweeter.app.model.TweetTag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TweetTagRepository extends JpaRepository<TweetTag, Long> {
    Optional<TweetTag> findByName(String name);
}
