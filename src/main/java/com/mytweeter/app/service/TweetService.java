package com.mytweeter.app.service;

import com.mytweeter.app.mapper.TweetMapper;
import com.mytweeter.app.model.Tweet;
import com.mytweeter.app.model.TweetTag;
import com.mytweeter.app.model.dto.AddTagToTweetDto;
import com.mytweeter.app.model.dto.TweetDto;
import com.mytweeter.app.repository.TweetRepository;
import com.mytweeter.app.repository.TweetTagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class TweetService {
    @Autowired
    private TweetRepository tweetRepository;

    @Autowired
    private TweetTagRepository tweetTagRepository;

    @Autowired
    private TweetMapper tweetMapper;

    public Optional<TweetDto> create(TweetDto dto) {
        Tweet tweet = tweetMapper.tweetDtoToNewTweet(dto);
        tweet = tweetRepository.save(tweet);

        return Optional.of(tweetMapper.tweetToTweetDto(tweet));
    }

    public List<TweetDto> getAll() {
        return tweetRepository
                .findAll()
                .stream()
                .map(tweet -> tweetMapper.tweetToTweetDto(tweet))
                .collect(Collectors.toList());
    }

    public Optional<TweetDto> getById(Long id) {
        Optional<Tweet> tweetOptional = tweetRepository.findById(id);
        return tweetOptional.map(tweet -> tweetMapper.tweetToTweetDto(tweet));
    }

    public void update(TweetDto dto) {
        Tweet tweet = tweetMapper.tweetDtoToTweet(dto);

        tweetRepository.save(tweet);
    }

    public void remove(Long id) {
        tweetRepository.deleteById(id);
    }

    public void addTagToTweet(AddTagToTweetDto dto) {
        Optional<Tweet> optionalTweet = tweetRepository.findById(dto.getTweetId());
        if (optionalTweet.isPresent()) {
            Tweet tweet = optionalTweet.get();
            TweetTag tag = getTag(dto.getTagName());

            tweet.getTagSet().add(tag);
            tweetRepository.save(tweet);

            tag.getTweetSet().add(tweet);
            tweetTagRepository.save(tag);
        }
    }

    public TweetTag getTag(String name) {
        Optional<TweetTag> tagOptional = tweetTagRepository.findByName(name);
        if (tagOptional.isPresent()) {
            return tagOptional.get();
        }
        TweetTag tag = new TweetTag();
        tag.setName(name);

        tag = tweetTagRepository.save(tag);
        tag = tweetTagRepository.findById(tag.getId()).get();
        return tag;
    }

    public List<TweetDto> findTweetsWithTag(String tagName) {
        TweetTag tag = getTag(tagName);
        return tag.getTweetSet()
                .stream()
                .map(tweet -> tweetMapper.tweetToTweetDto(tweet))
                .collect(Collectors.toList());
    }
}
