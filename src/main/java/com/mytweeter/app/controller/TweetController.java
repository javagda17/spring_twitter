package com.mytweeter.app.controller;

import com.mytweeter.app.model.Type;
import com.mytweeter.app.model.dto.AddTagToTweetDto;
import com.mytweeter.app.model.dto.TweetDto;
import com.mytweeter.app.service.TweetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class TweetController {

    @Autowired
    private TweetService tweetService;

    //    @GetMapping("/addForm")
    @RequestMapping(value = "/addForm", method = RequestMethod.GET)
    public String getTweetForm(Model model) {
        model.addAttribute("tweetOptions", Type.values());
        model.addAttribute("tweetList", tweetService.getAll());
        return "tweetForm";
    }

    //    @PostMapping("/addForm")
    @RequestMapping(value = "/addForm", method = POST)
    public String postTweetForm(TweetDto dto) {
        tweetService.create(dto);

        return "redirect:/addForm";
    }

    //    @GetMapping("/list")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String getTweetList(Model model) {
        model.addAttribute("tweetList", tweetService.getAll());
        return "list";
    }

    @RequestMapping(value = "/edit/{identifier}", method = RequestMethod.GET)
    public String getEditForm(Model model, @PathVariable(name = "identifier") Long id) {
        Optional<TweetDto> tweetDtoOptional = tweetService.getById(id);
        if (tweetDtoOptional.isPresent()) {
            model.addAttribute("tweetOptions", Type.values());
            model.addAttribute("tweetToEdit", tweetDtoOptional.get());
            return "tweetEditForm";
        }

        return "redirect:/addForm";
    }

    @RequestMapping(value = "/edit/{identifier}", method = POST)
    public String getEditForm(Model model, @PathVariable(name = "identifier") Long id,
                              TweetDto dto) {
        tweetService.update(dto);

        return "redirect:/addForm";
    }

    @RequestMapping(value = "/remove", method = RequestMethod.GET)
    public String removeTweet(@RequestParam(name = "tweetToRemoveId") Long id) {
        tweetService.remove(id);

        return "redirect:/addForm";
    }

    @RequestMapping(value = "/tweetTag/", method = POST)
    public String addTagToTweet(AddTagToTweetDto dto) {
        tweetService.addTagToTweet(dto);

        return "redirect:/edit/" + dto.getTweetId();
    }

    @RequestMapping(value = "/listTweets/{tagName}")
    public String getTweetsWithTag(Model model, @PathVariable(name = "tagName") String tagName){
        model.addAttribute("tweetList", tweetService.findTweetsWithTag(tagName));

        return "list";
    }
}
