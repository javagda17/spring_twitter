package com.mytweeter.app.model;

public enum Status {
    ORIGINAL,
    EDITED
}
