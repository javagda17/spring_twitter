package com.mytweeter.app.model;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Tweet {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Length(max = 160)
    private String text;
    // ConstraintViolationException

    @CreationTimestamp
    private LocalDateTime created;

    private Status status;
    private Type type;

    @ManyToMany(mappedBy = "tweetSet", fetch = FetchType.EAGER)
    private Set<TweetTag> tagSet;
}
