package com.mytweeter.app.model.dto;

import com.mytweeter.app.model.Status;
import com.mytweeter.app.model.TweetTag;
import com.mytweeter.app.model.Type;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDateTime;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TweetDto {
    private Long tweetId;
    private String tweetContent;
    private LocalDateTime tweetCreationTime;
    private Status tweetStatus;
    private Type tweetType;
    private Set<TweetTag> tagSet;
}
