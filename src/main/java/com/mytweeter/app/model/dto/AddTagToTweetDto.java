package com.mytweeter.app.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddTagToTweetDto {
    private Long tweetId;
    private String tagName;
}
