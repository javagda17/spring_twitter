package com.mytweeter.app.model;

public enum Type {
    TWEET,
    COMMENT,
    FORWARDED_TWEET
}
